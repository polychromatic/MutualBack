// Require gulp package
var gulp            = require('gulp');
var uglify          = require('gulp-uglify');
var typescript      = require('gulp-tsc');

// ---------- UGLIFY ------------- >
gulp.task('main-uglify', function() {
  return gulp.src('core/server.js')
    .pipe(uglify())
    .pipe(gulp.dest('./core'));
});

// ---------- COMPILE TYPESCRIPT TO JS ------------- >
gulp.task('compile-server', function(){
  gulp.src(['core/server.ts'])
    .pipe(typescript())
    .pipe(gulp.dest('./core'))
});

gulp.task('compile',['compile-server']);
gulp.task('default',['compile','uglify']);
gulp.task('uglify',['main-uglify']);
