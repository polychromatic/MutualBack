var Async = require('async');
var Q = require("Q");
var sites = require('./configs/sites.json');
var chalk = require('chalk');
var fs = require('fs');
var rf = require('rimraf');
var path = require('path');

var param = process.argv[2] || "-u";
var opt = process.argv[3];

var colors = {
    err : chalk.bold.red,
    info : chalk.bold.cyan,
    warning : chalk.bold.yellow,
    ok : chalk.bold.green,
    bulle : chalk.bold.magenta
}

var shell = require('shelljs/global');
var rootGit = require('simple-git')('./');

function checkPath(s){
    try {return fs.realpathSync(s)} catch(e){return false;}
}

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

if(param == "-u") {
    if(opt == undefined) {

        Async.eachSeries(sites.running, function iterator(o, callback) {
            var item = sites.sites[o];
            if(checkPath("./sites/"+o) === false) {
                console.log(colors.info("Projet inexistant, début du clonage de => "+colors.warning(o)));
                rootGit.clone(item.repo_https,'./sites/'+o,function() {
                    console.log(colors.ok("     => Clonage du répertoire réussie avec succès !"));
                    callback();
                });
            }
            else {
                console.log(colors.info("Projet => ")+colors.warning(o)+colors.info(" déjà installé, pull du répertoire!"));
                require('simple-git')('./sites/'+o).pull(function(err, update) {
                    if(update && update.summary.changes) {
                        console.log(colors.ok("     => Le répertoire est désormais à jour !"));
                        callback();
                    }
                    else {
                        console.log(colors.warning("     => Aucune mise à jour pour le répertoire !"));
                        callback();
                    }
                });
            }
        }, function done() {
            console.log("");
            console.log(colors.bulle("All sites installed and updated!"));
            process.exit();
        });

    }
    else {
        var updateProject = function() {
            var deffered = Q.defer();
            if(checkPath("./sites/"+opt) === false) {
                if(sites.sites[opt] != undefined) {
                    var env = sites.sites[opt];
                    console.log(colors.info("Projet inexistant, début du clonage de => "+colors.warning(opt)));
                    rootGit.clone(env.repo_https,'./sites/'+opt,function() {
                        console.log(colors.ok("     => Clonage du répertoire réussie avec succès !"));
                        deffered.resolve(true);
                    });
                }
                else {
                    console.log(colors.err("Nom de répertoire incorrect!"));
                    deffered.reject(false);
                }
            }
            else {
                console.log(colors.info("Mise à jour du répertoire => ")+colors.warning(opt));
                require('simple-git')('./sites/'+opt).pull(function(err, update) {
                    if(update && update.summary.changes) {
                        console.log(colors.ok("     => Le répertoire est désormais à jour !"));
                        deffered.resolve(true);
                    }
                    else {
                        console.log(colors.warning("     => Aucune mise à jour pour le répertoire !"));
                        deffered.resolve(true);
                    }
                });
            }
            return deffered.promise;
        }

        updateProject().then(function(res){
            process.exit();
        });
    }
}
else if(param == "-r") {
    if(opt == undefined) {
        console.log(colors.info("Suppression de tout les dossiers sites."));
        var directories = getDirectories('./sites');
        for(var k in directories) {
            rf("./sites/"+directories[k],function(err) {
                if(err)
                    console.log(err);
            });
        }
    }
    else {
        if(checkPath("./sites/"+opt) === false) {
            console.log(colors.err("Nom de répertoire incorrect!"));
        }
        else {
            console.log(colors.info("Suppression du dossier => ")+colors.warning(opt));
            rf("./sites/"+opt,function(err) {
                if(err)
                    console.log(err);
            });
        }
    }
}
