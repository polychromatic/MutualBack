var commands = require('./configs/datacommands.json');
var chalk = require('chalk');
var colors = {
    err : chalk.bold.red,
    info : chalk.bold.cyan,
    warning : chalk.bold.yellow,
    ok : chalk.bold.green,
    bulle : chalk.bold.magenta
}
var param = process.argv[2] || "help";
var shell = require('shelljs/global');

function Stringtask(t) {
    var minified = "[";
    var count = 1;
    for(var k in t) {
        if(count == 1)
            minified += k.toString();
        else
            minified += "|"+k.toString();
        count++;
    }
    return minified += "]";
}

var help = function() {
    console.log(colors.info("Commande disponible : ")+colors.ok(Stringtask(commands.crontabs)));
    process.exit();
}

// Switch param !
switch(param) {
    case "help":
        help();
    break;
    case "start":
        exec("npm start",{silent: false});
    break;
}

var focusTasks = commands.crontabs[param];
if(focusTasks != undefined) {

    var repportingError = [];
    var errorCount = 0;

    // Require dependencies !
    var Q = require('q');
    var async = require('async');

    async.eachSeries(focusTasks, function iterator(o, callback) {
        console.log(colors.warning("-----------------------------------------------------------------"));
        var item = commands.tasks[o]
        var timer = Date.now();
        console.log(colors.warning("Start : ")+colors.info(item.title));
        var silent = (item.silent == undefined) ? true : item.silent;
        exec(o,{silent: silent},function(code,outpout) {
            timer = (Date.now() - timer)/1000;
            var state = (code == 0) ? colors.ok("[Success] ") : colors.err("[Error] ");
            if(code == 1) {
                repportingError.push({
                    task : item.title,
                    err : outpout
                });
                errorCount++;
            }
            console.log(colors.warning("End : ")+state+colors.info(item.title)+colors.warning(" ["+timer+"s]"));
            callback();
        });
    }, function done() {
        console.log(colors.warning("-----------------------------------------------------------------"));
        console.log(colors.bulle("Toutes les tâches sont terminés avec : ")+colors.warning(errorCount+" erreur(s)"));
        if(repportingError.length > 0) {
            console.log(colors.err("Affichage du rapport d'erreurs : "));
            console.log(colors.info("-----------------------------------------------------------------"));
            for(var k in repportingError) {
                var v = repportingError[k];
                console.log(colors.info("Task => ")+colors.warning(v.task));
                console.log(colors.info("Outpout message => ")+colors.warning(v.err));
                console.log(colors.info("-----------------------------------------------------------------"));
            }
        }
        process.exit();
    });

}
else {
    console.log(colors.err("Le nom de votre commande n'est pas présente dans le registre!"));
    help();
}
