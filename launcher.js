var async = require('async');
var servers = require('./configs/servers.json');
var chalk = require('chalk');
var colors = {
    err : chalk.bold.red,
    info : chalk.bold.cyan,
    warning : chalk.bold.yellow,
    ok : chalk.bold.green,
    bulle : chalk.bold.magenta
}
var shell = require('shelljs/global');

async.eachSeries(servers.running, function iterator(o, callback) {
    var item = servers.servers[o];
    console.log(colors.info("Started new server named => "+colors.warning(o)+" on port => "+colors.warning(item.port)))
    exec("node core/server "+o,{silent: false,async: true});
    callback();
}, function done() {
    console.log(colors.ok("=> ---- All servers started ---- <="));
});
