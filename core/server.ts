///<reference path='../typings/tsd.d.ts' />

// Require NodeJS dependencies
let servers = require('../configs/servers.json');
let sites = require('../configs/sites.json').running;
let ProcessName : string = process.argv[2];
let fs = require('fs');
let config = servers.servers[ProcessName];
let path = require('path');
let Async = require('async');
var chalk = require('chalk');
var colors = {
    err : chalk.bold.red,
    info : chalk.bold.cyan,
    warning : chalk.bold.yellow,
    ok : chalk.bold.green,
    bulle : chalk.bold.magenta
}

if(config == undefined) {
    console.log(colors.err("Fatal error, missing server configuration!"));
    process.exit();
}

// Express et Configuration.json
let express     = require('express');
let app         = express();
let bodyparser  = require("body-parser");

// Configure express sample
app.use(express.static( path.join(__dirname,'../public') ));

app.set('views',path.join(__dirname,'../views'));
app.set('view engine','jade');

// Routing
app.get('/',function(req,res) {
    res.render("layout",{name : ProcessName,sites: sites});
});

app.get('/admin',function(req,res) {
    res.send("Administration area");
});

Async.eachSeries(sites, (o, callback) => {
    let site_configuration = require(`../sites/${o}/configuration.json`);
    app.use(site_configuration.path_file,express.static( path.join(__dirname,`../sites/${o}/public`) ));
    app.use(`/${o}`,require(`../sites/${o}/main.js`));
    callback();
}, () => {
    console.log(colors.info("   => all routes loaded on server => ")+colors.warning(ProcessName));
});

let server = app.listen(config.port,config.ip);
